<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PaymentTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function test_cash_payment(): void
    {
        $data = [
            'usedCash' => [
                ['value' => 5, 'quantity' => rand(0, 5)],
                ['value' => 10, 'quantity' => rand(0, 5)],
                ['value' => 20, 'quantity' => rand(0, 5)],
                ['value' => 50, 'quantity' => rand(0, 5)],
                ['value' => 100, 'quantity' => rand(0, 5)],
                ['value' => 200, 'quantity' => rand(0, 5)],
                ['value' => 500, 'quantity' => rand(0, 5)],
                ['value' => 1000, 'quantity' => rand(0, 5)],
                ['value' => 2000, 'quantity' => rand(0, 5)],
                // Add other mock data as needed
            ],
            'calculatedPrice' => rand(0, 250),
        ];

        // Send request to the endpoint
        $response = $this->put('/machine/1/shopping/cash-payment', $data);

        // Assert response status code
        $response->assertStatus(200);
    }
}
