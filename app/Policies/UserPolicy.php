<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;
use App\Services\FlashMessageService;

class UserPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return  $user->role->name === config('roles.employee') || $user->role->name === config('roles.admin');

    }

    /**
     * Determine whether the user can update the models role attribute.
     */
    public function updateUserRole(User $user): bool
    {
        return  $user->role->name === config('roles.admin');
    }
}
