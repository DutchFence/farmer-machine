<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StorageProperty extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'type',
    ];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
