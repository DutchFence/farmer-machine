<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Coin extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'value'
    ];

    public function machines()
    {
        return $this->belongsToMany(Machine::class)
            ->withPivot('quantity');
    }
}
