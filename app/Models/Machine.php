<?php

namespace App\Models;

use App\Models\Coin;
use App\Models\User;
use App\Models\StorageSpace;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Machine extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'user_id',
    ];

    public function getCurrentCoins(): array
    {
        return array_reverse($this->coins()->withPivot('quantity')->get()->toArray());
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function storage_spaces(): HasMany
    {
        return $this->hasMany(StorageSpace::class);
    }

    public function coins(): BelongsToMany
    {
        return $this->belongsToMany(Coin::class)
            ->withPivot('quantity'); // Specify the pivot column
    }
}
