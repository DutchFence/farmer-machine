<?php

namespace App\Models;

use App\Models\StorageSpace;
use App\Models\PaymentProduct;
use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    protected $fillable = [
        'price',
    ];
    
    use HasFactory;

    public function category(): BelongsTo
    {
        return $this->belongsTo(ProductCategory::class);
    }
    
    public function storage_space(): HasMany{
        return $this->hasMany(StorageSpace::class);
    }
  
    public function users(): BelongsToMany
{
    return $this->belongsToMany(User::class, 'user_product_prices')
    ->withPivot('price');
}
public function price(): HasOne
    {
        return $this->hasOne(Price::class);
    }
}

