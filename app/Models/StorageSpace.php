<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class StorageSpace extends Model
{
    use HasFactory;

    protected $fillable = [
        'storage_property_id',
        'product_id',
        'machine_id',
        'is_open',
        'is_filled'
    ];
    
    public function product(): HasOne
    {
        return $this->hasOne(Product::class);
    }

    public function machine(): BelongsTo
    {
        return $this->belongsTo(Machine::class);
    }
}
