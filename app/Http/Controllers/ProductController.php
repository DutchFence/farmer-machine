<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        $categories = ProductCategory::all();
        $products = Product::with(['price' => function ($query) use ($user) {
            $query->where('user_id', $user->id);
        }])->get();
        return Inertia::render('User/Products', [
            'products' => $products,
            'categories' => $categories
        ]);
    }
}
