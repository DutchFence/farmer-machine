<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Services\FlashMessageService;
use App\Http\Requests\CashRequest;
use Illuminate\Support\Facades\Auth;

class CashController extends Controller
{
    /**
     * Display the specified resource.
     */

    public function show($id)
    {
        $user = Auth::user();
        $machine = $user->machines->find($id);
        $coins = $machine->coins()->withPivot('quantity')->get(); 
        return Inertia::render('User/ManageCash', [
            'machine' => $machine,
            'coins' => $coins,
        ]);
    }

    /**
     * Set the coins for the specified machine
     */
    public function exchange(CashRequest $cashRequest, $id)
    {
        $user = Auth::user();
        $machine = $user->machines->find($id);
    
        if (!$machine) {
            return back()->with(FlashMessageService::fail());
        }
        $coins = $machine->coins;
        $validatedData = $cashRequest->validated();
        // Iterate through the coins and update their quantities
        foreach ($coins as $coin) {
            $coinId = $coin->id;
            if (isset($validatedData[$coinId])) {
                $quantity = $validatedData[$coinId]['quantity'];
                $machine->coins()->updateExistingPivot($coinId, ['quantity' => $quantity]);
            }
        }
        return back()->with(FlashMessageService::success("Wisselgeld is aangepast"));
    }
    
}
