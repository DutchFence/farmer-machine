<?php

namespace App\Http\Controllers;

use Exception;
use Inertia\Inertia;
use App\Models\Machine;
use App\Models\Product;
use App\Models\StorageSpace;
use App\Models\StorageProperty;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Services\FlashMessageService;
use App\Http\Requests\FillCashRequest;
use Symfony\Component\HttpFoundation\Request;

class ManageMachineController extends Controller
{
    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $user = Auth::user();
        $machine = $user->machines->find($id);

        if (!$machine) {
            return back()->with([
                'message' => 'De machine kon niet worden gevonden.'
            ]);
        }

        $storageSpaces = $machine->storage_spaces->toArray();
        $productIds = array_values(array_column($storageSpaces, 'product_id'));
        $productIds = array_filter($productIds, function ($id) {
            return $id !== null;
        });
        
        $storageProperties = StorageProperty::all();
        try {
            $products = Product::with(['price' => function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                }])
                ->get();
        } catch (Exception $e) {
            Log::error('Error during the retrieval of the products and prices of the user: ' . $e);
            back()->with(FlashMessageService::fail());
        }
        return Inertia::render('User/ManageMachine', [
            'machine' => $machine,
            'storages' => $storageSpaces,
            'products' => $products,
            'storageTypes' => $storageProperties,
        ]);
    }

    public function switchProduct(string $storageId, Request $request)
    {
        $storageSpace = StorageSpace::find($storageId);
        $product_id = $request['product_id'];
        $product = Product::find($product_id);

        $productStorageTypeId = $product->storage_property_id;
        $storageStorageTypeId = $storageSpace->storage_property_id;

        if ($productStorageTypeId !== $storageStorageTypeId) {
            $storageType = StorageProperty::find($storageStorageTypeId);
            $productStorageType = StorageProperty::find($productStorageTypeId);
            return back()->with(FlashMessageService::fail("Het product " . $product->name . " hoort niet in een "
             . $storageType->type .  " vak, maar in een " . $productStorageType->type . " vak"));
        }
        if ($storageSpace->is_filled) {
            return back()->with(FlashMessageService::fail("Maak eerst het vak leeg, voordat u het product wisselt"));
        }
        $storageSpace->product_id = $product_id;
        $storageSpace->save();
        return back();
    }
    public function latch(string $storageId)
    {
        $user = Auth::user();
        $storageSpace = StorageSpace::find($storageId);

        if ($storageSpace) {
            $machineId = $storageSpace->machine_id;
            $isOpen = $storageSpace->is_open;
            $isPaid = $storageSpace->paid;
            if ($isPaid === 1) {
                $storageSpace->paid = 0;
                $storageSpace->save();
            }
        } else {
            return back()->with(FlashMessageService::fail());
        }
        $machine = Machine::find($machineId);
        if ($machine->user_id === $user->id) {
            $storageSpace->is_open = !$isOpen;
            $storageSpace->save();
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     */
    public function stock(string $storageId)
    {

        $user = Auth::user();
        $storageSpace = StorageSpace::find($storageId);

        if ($storageSpace) {
            if(!$storageSpace->product_id){
                return back()->with(FlashMessageService::fail('Dit vak heeft nog geen product'));
            }
            $machineId = $storageSpace->machine_id;
            $isFilled = $storageSpace->is_filled;
        } else {
            return back()->with(FlashMessageService::fail());
        }
        $machine = Machine::find($machineId);
        if ($machine->user_id === $user->id) {
            $storageSpace->is_filled = !$isFilled;
            $storageSpace->save();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function fillCash(FillCashRequest $fillCashRequest, string $id)

    {
        $user = Auth::user();
        $machine = $user->machines->find($id);
        $cash = $machine->cash;
        $cash->current_amount += $fillCashRequest->extra_amount;
        $cash->save();
    }
}
