<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Services\FlashMessageService;
use App\Http\Requests\CardPaymentRequest;
use App\Http\Requests\CashPaymentRequest;
use Illuminate\Http\RedirectResponse;

class PaymentController extends Controller
{
     /**
     * Round a sum down in the same manner as with money
     */
    protected function roundCash(float $sum): float
    {
        $roundedToTwo = round($sum, 2);
        $roundedString = number_format($roundedToTwo, 2, '.', '');
        $lastDigit = substr($roundedString, -1);
        if ($lastDigit == 1 || $lastDigit == 2) {
            $newSum = round($roundedToTwo, 1);
        } elseif ($lastDigit == 8 || $lastDigit == 9) {
            $newSum = round($roundedToTwo, 1);
        } else {
            $modifiedString = substr($roundedString, 0, -1) . '5';
            $newSum = (float) $modifiedString;
        }
        return $newSum;
    }

    /**
     * Handle a payment by cash
     */
    public function cash(CashPaymentRequest $cashPaymentRequest, $machine_id): void
    {
        DB::transaction(function () use ($cashPaymentRequest, $machine_id) {
            $user = Auth::user();
            $machine = $user->machines->find($machine_id);
            $data = $cashPaymentRequest->validated();
            $usedCash = $data['usedCash'];
            $selectedStorages = $data['storages'];
            $totalPrice = $data['totalPrice'];
            $totalCash = 0;
            // Calculate the balance to return
            foreach ($usedCash as $coin) {
                $totalCash += ($coin['quantity'] * $coin['value']) / 100;
            }
            $rawBalance = round(round($totalCash, 2) - round($totalPrice, 2), 2);
            $balance = $this->roundCash($rawBalance);
            // Calculate how much cash to return
            $coins = $machine->getCurrentCoins();
            while ($balance > 0) {
                foreach ($coins as &$coin) {
                    if ($balance == 0.0) {
                        break;
                    }
                    while ($coin['pivot']['quantity'] > 0) {
                        $value = (float) $coin['value'] / 100.00;
                        $balanceCheck = (float) $balance - $value;
                        if ($balanceCheck < 0) {
                            break;
                        }
                        $balance = number_format($balanceCheck, 2);
                        $coin['pivot']['quantity'] -= 1;
                    }
                }
                if ($balance != 0) {
                    return back()->with(FlashMessageService::fail("Betaling mislukt, automaat heeft onvoldoende wisselgeld"));
                }
            }
            // Set the storages to paid
            foreach ($selectedStorages as $storage) {
                $customerController = new CustomerController();
                $customerController->payStorage($machine_id, $storage['id']);
            }
            // Return change
            foreach (array_reverse($coins) as $coinData) {
                $machine->coins()->updateExistingPivot($coinData['id'], ['quantity' => $coinData['pivot']['quantity']]);
            }
            // Store the customers money into the machine
            foreach ($data['usedCash'] as $coinData) {
                $currentQuantity = $machine->coins()->where('value', $coinData['value'])->first()->pivot->quantity;
                $newQuantity = $currentQuantity + $coinData['quantity'];
                $machine->coins()->updateExistingPivot($coinData['id'], ['quantity' => $newQuantity]);
            }
            return back()->with(FlashMessageService::success("Uw betaling is geslaagd"));
        });
    }

    /**
     * Handle a payment by card
     */
    public function card(CardPaymentRequest $cardPaymentRequest, $machine_id): RedirectResponse
    {
        $data = $cardPaymentRequest->validated();
        $selectedStorages = $data['storages'];
        // Set the storages to paid
        try {
            foreach ($selectedStorages as $storage) {
                $customerController = new CustomerController();
                $customerController->payStorage($machine_id, $storage['id']);
            }
        } catch (Exception $e) {
            Log::error("Error tijden het betalen met de pin: => " . $e);
            return back()->with(FlashMessageService::fail("Betaling is mislukt, neem contact op met ons"));
        }
        return back()->with(FlashMessageService::success("Uw betaling is geslaagd"));
    }
}
