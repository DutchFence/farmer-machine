<?php

namespace App\Http\Controllers;

use App\Models\Price;
use App\Services\FlashMessageService;
use App\Http\Requests\UpdatePriceRequest;

class ProductPriceController extends Controller
{
  /**
   * Update the specified resource in storage.
   */
  public function update(UpdatePriceRequest $request, $id)
  {
    $user = $request->user();
    $product_id = $id;
    $rawPrice = $request->price;
    $price = (float) str_replace(',', '.', $rawPrice);
    $usersProduct = $user->prices->find($product_id);
    if (!$usersProduct) {
      $create =  Price::create([
        'product_id' => $product_id,
        'user_id' => $user->id,
        'price' => $price
      ]);
      if (!$create) {
        return back()->with(FlashMessageService::fail('Het aanmaken van de prijs is mislukt, probeer het opnieuw of neem contact met ons op'));
      }
    } else {
      $update =  $usersProduct->pivot->update(['price' => $price]);
      if ($update == 0) {
        return back()->with(FlashMessageService::fail('Het aanpassen van de prijs is mislukt, probeer het opnieuw of neem contact met ons op'));
      }
      return back()->with(FlashMessageService::successEdit("De prijs"));
    }
  }
}
