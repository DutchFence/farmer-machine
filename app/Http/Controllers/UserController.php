<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Inertia\Inertia;
use App\Services\FlashMessageService;
use App\Http\Requests\UpdateUserRoleRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {  
        $users = User::select('id', 'name', 'email', 'role_id')
        ->get();
        $roles = Role::select('id', 'name')->get();
        return Inertia::render('Staff/UserOverviewPage', [
            'users' => $users,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified role in of the specified role.
     */
    public function updateRole(UpdateUserRoleRequest $request, string $userId)
    { 
        $this->authorize('updateUserRole', User::class);

        $newRoleId = $request->newRoleId;
        $oldRoleId = $request->oldRoleId;
        if ($oldRoleId === $newRoleId) {
            return back();
        }
        $user = User::findOrFail($userId);
        $user->role_id = $newRoleId;
        $user->save();
        return back()->with(FlashMessageService::successEdit('De rol'));
    }
}
