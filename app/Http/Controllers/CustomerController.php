<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Services\FlashMessageService;

class CustomerController extends Controller
{

    /**
     * Open the specified storage and set paid attribute to true
     */
    public function payStorage(string $machineId, string $storageId)
    {
        $user = Auth::user();
        $machine = $user->machines->find($machineId);
        $storage = $machine->storage_spaces->find($storageId);
        if ($machine && $storage) {
            $storage->paid = true;
            $storage->save();
            return back();
        } else {
            return back()->with(FlashMessageService::fail("Het vak is niet gevonden"));
        }

    }
}
