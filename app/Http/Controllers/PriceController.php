<?php

namespace App\Http\Controllers;

use App\Services\FlashMessageService;
use App\Http\Requests\UpdatePriceRequest;

class PriceController extends Controller
{
    public function update(UpdatePriceRequest $request, $id)
    {
        $user = $request->user();
        $product_id = $id;
        $rawPrice = $request->price;
        $price = (float) str_replace(',', '.', $rawPrice);
    
        $userProduct = $user->products()->where('products_id', $product_id)->first();
    
        if (!$userProduct || !$userProduct->pivot->price) {
            // If the user dosn't have a price related to the product or the price is not set, create a new price record
            $user->products()->syncWithoutDetaching([$product_id => ['price' => $price]]);
        } else {
            // If the user already has a price related to the product, update the existing price record
            $user->products()->updateExistingPivot($product_id, ['price' => $price]);
        }
    
        return back()->with(FlashMessageService::success());
    }
    
}
