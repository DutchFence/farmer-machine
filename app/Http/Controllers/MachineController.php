<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Coin;
use Inertia\Inertia;
use App\Models\Machine;
use App\Models\Product;
use App\Models\StorageSpace;
use App\Models\StorageProperty;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreMachineRequest;
use Illuminate\Database\UniqueConstraintViolationException;
use App\Services\FlashMessageService;

class MachineController extends Controller
{
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user = Auth::User();
        $machines = $user ? $user->machines : [];
        $products = Product::with(['price' => function ($query) use ($user) {
            $query->where('user_id', $user->id);
        }])->get();
        $storageProperties = StorageProperty::all();
        return Inertia::render('User/CreateMachine', [
            'products' => $products,
            'machines' => $machines,
            'storageTypes' => $storageProperties
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * Input:: Data object with properties:
     * name, storages (array of objects)
     */
    public function store(StoreMachineRequest $request)
    {
        try {
            $data = $request->validated();
            DB::transaction(function () use ($data) {
                $user = Auth::user();
                $name = $data['name'];
                $machine = Machine::create([
                    'name' => $name,
                    'user_id' => $user->id,
                ]);
                $coins = Coin::all();
                // Loop through coins and associate them with the machine
                foreach ($coins as $coin) {
                    $machine->coins()->attach($coin, ['quantity' => 0]);
                }
                // Create storage spaces for the machine if provided
                if (isset($data['storages'])) {
                    foreach ($data['storages'] as $storage) {
                        $product_id = $storage['product_id'] ?? null;
                        $property_id = $storage['type'];
                        StorageSpace::create([
                            'product_id' => $product_id,
                            'storage_property_id' => $property_id,
                            'machine_id' => $machine->id,
                        ]);
                    }
                }
            });
            return redirect('/dashboard')->with(FlashMessageService::successStore('Uw machine ' . $data["name"] . ' '));
        } catch (UniqueConstraintViolationException $e) {
            Log::error('Error when storing a new machine: ' . $e);
            return back()->with(FlashMessageService::failDuplicate("naam"));
        }
    }
    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $user = Auth::user();
        $machine = $user->machines->find($id);
        if (!$machine) {
            back()->with(FlashMessageService::fail("De machine is niet gevonden"));
        }
        $storageSpaces = $machine->storage_spaces;
        $storagePropertyIds = collect();
        foreach ($storageSpaces as $storageSpace) {
            $storagePropertyIds->add($storageSpace->storage_property_id);
        }
        $storagePropertyIds = $storagePropertyIds->unique();
        $storageProperties = StorageProperty::whereIn('id', $storagePropertyIds)->get();
        try {
            $products = Product::with(['price' => function ($query) use ($user) {
                $query->where('user_id', $user->id);
            }])->get();
        } catch (Exception $e) {
            Log::error('Error during the retrieval of the products and prices of the user: ' . $e);
            return back()->with(FlashMessageService::failDuplicate("Er is iets mis gegaan"));
        }

        return Inertia::render('User/Machine', [
            'machine' => $machine,
            'storages' => $storageSpaces,
            'products' => $products,
            'storageTypes' => $storageProperties,
        ]);
    }
}
