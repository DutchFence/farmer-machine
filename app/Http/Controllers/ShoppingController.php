<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Coin;
use Inertia\Inertia;
use App\Models\Product;
use App\Models\StorageProperty;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Services\FlashMessageService;

class ShoppingController extends Controller
{
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = Auth::user();
        $machine = $user->machines->find($id);
        $coins = Coin::all();
        $storageProperties = StorageProperty::all();
        if (!$machine) {
            back()->with(FlashMessageService::fail("Machine is niet gevonden"));
        }
        $storageSpaces = $machine->storage_spaces;
        try {
            $products = Product::with(['price' => function ($query) use ($user) {
                $query->where('user_id', $user->id);
            }])->get();
        } catch (Exception $e) {
            Log::error('Error during the retrieval of the products and prices of the user: ' . $e);
            back()->with(FlashMessageService::fail());
        }
        return Inertia::render('User/ShoppingPage', [
            'machine' => $machine,
            'storages' => $storageSpaces,
            'products' => $products,
            'coins' => $coins,
            'storageTypes' => $storageProperties,
        ]);
    }
}
