<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::User();
        $machines = $user->machines;
        return Inertia::render('User/Dashboard', [
            'machines' => $machines,
        ]);
    }
}
