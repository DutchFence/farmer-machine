<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CardPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $machineIdFromUrl = $this->route('id');
        return [
            'totalPrice' => 'required|numeric', 
            'storages' => 'required|array',
            'storages.*.is_filled' => 'required|boolean|in:1',
            'storages.*.is_open' => 'required|boolean|not_in:1',
            'storages.*.id' => 'required|integer',
            'storages.*.machine_id' => 'required|integer|in:' . $machineIdFromUrl,
        ];
    }
}
