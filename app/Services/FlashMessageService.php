<?php

namespace App\Services;

class FlashMessageService {

    public static function success( $message = "Het is gelukt", $status = "success"){
        return [
            'flash' => [
                'message' => $message,
                'status' => $status,
            ]
            ];
    }
    public static function successStore($waarde = null, $message = "Het is succesvol aangemaakt", $status = "success"){
        if($waarde){
            $message = $waarde . " is succesvol aangemaakt";
        }        return [
            'flash' => [
                'message' => $message,
                'status' => $status,
            ]
            ];
    }
    
    public static function successEdit($waarde = null, $message = "Het is succesvol aangepast", $status = "success"){
        if($waarde){
            $message = $waarde . " is succesvol aangepast";
        }
        return [
            'flash' => [
                'message' => $message,
                'status' => $status,
            ]
            ];
    }
    public static function failDuplicate($waarde = null, $message = "Uw keuze bestaat al", $status = "fail" ){
        if($waarde){
            $message = "Uw gekozen " . $waarde . " bestaat al";
        }
        return [
            'flash' => [
                'message' => $message,
                'status' => $status,
            ]
            ];
    }
    public static function fail($message = "Het is mislukt", $status = "fail"){
        return [
            'flash' => [
                'message' => $message,
                'status' => $status,
            ]
            ];
    }
    public static function unauthorized($message = "U bent niet geauthoriseerd", $status = "fail"){
        return [
            'flash' => [
                'message' => $message,
                'status' => $status,
            ]
            ];
    }
}