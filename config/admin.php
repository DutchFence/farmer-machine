<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings for the admin account
    |--------------------------------------------------------------------------
    |
    |The password, email and name for the admin account that can be adjusted in the .env file or here
    |
    */
    'name' => env('ADMIN_NAME', 'Adminman'),
    'email' => env('ADMIN_EMAIL', 'adminman@gmail.com'),
    'password' => env('ADMIN_PASSWORD', 'admin1234!'),
];
