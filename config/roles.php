<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Names for the roles
    |--------------------------------------------------------------------------
    |
    |The roles and their corresponding names, that can be adjusted in the .env file or here
    |
    */
    'admin' => env('ADMIN_ROLE', 'administrator'),
    'employee' => env('EMPLOYEE_ROLE', 'werknemer'),
    'customer' => env('CUSTOMER_ROLE', 'klant'),
];
