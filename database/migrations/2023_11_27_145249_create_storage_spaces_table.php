<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('storage_spaces', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->boolean('is_filled')->default(false);
            $table->boolean('is_open')->default(false);
            $table->boolean('paid')->default(false);
            $table->foreignId('product_id')->nullable();
            $table->foreignId('machine_id');
            $table->foreignId('storage_property_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('storage_spaces');
    }
};
