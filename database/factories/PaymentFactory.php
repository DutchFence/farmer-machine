<?php

namespace Database\Factories;

use App\Models\PaymentMethod;
use App\Models\PaymentProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Payment>
 */
class PaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $payment_method_id = optional(PaymentMethod::inRandomOrder()->first())->id ??
        PaymentMethod::factory()->create()->id;
        return [
            'payment_date' => fake()->dateTime(),
            'total_amount' => 0.00,
            'payment_method_id' => $payment_method_id,
        ];
    }
}
