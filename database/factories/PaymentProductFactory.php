<?php

namespace Database\Factories;

use App\Models\Payment;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PaymentProduct>
 */
class PaymentProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $payment =  optional(Payment::inRandomOrder()->first()) ??
        Payment::factory()->create();

        $product = optional(Product::inRandomOrder()->first()) ??
            Product::factory()->create();
        $payment->update(['total_amount' => $payment->total_amount + $product->price]);

        
        return [
            'payment_id' => $payment->id,
            'product_id' => $product->id
        ];
    }
}
