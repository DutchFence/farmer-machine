<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Address>
 */
class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'house_number' => fake()->numberBetween(1, 500),
            'house_number_extension' => mt_rand(1,10) <=7 ? fake()->randomLetter() : null ,
            'street' => fake()->streetName(),
            'zip_code'=> fake()->postcode(),
            'city'=> fake()->city(),
            'province'=> fake()->state()
        ];
    }
}
