<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\StorageSpace>
 */
class StorageSpaceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $states = ['closed', 'payment in progress', 'open', 'filling in progress'];
        $state = $states[array_rand($states)];

        $is_filled = match ($state) {
            'payment in progress' => true,
            'closed' =>  fake()->boolean(80),
            default => false,
        };

        $cooling = fake()->boolean(80);
        if ($cooling) {
            $minTemp = fake()->randomFloat(1, 0, 6);
            $maxTemp = fake()->randomFloat(1, 7, 10);
        }

        $product_id = optional(Product::inRandomOrder()->first())->id ??
            Product::factory()->create()->id;
            
        return [
            'can_cool' => $cooling,
            'is_filled' => $is_filled,
            'min_temperature' => $minTemp ?? null,
            'max_temperature' => $maxTemp ?? null,
            'current_temperature' => $cooling ? fake()->randomFloat(1, $minTemp, $maxTemp) : null,
            'state' => $state,
            'product_id' => $product_id,
        ];
    }
}
