<?php

namespace Database\Factories;

use App\Models\Machine;
use App\Models\StorageSpace;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MachineStorageSpace>
 */
class MachineStorageSpaceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $machine_id = optional(Machine::inRandomOrder()->first())->id ??
            Machine::factory()->create()->id;
        $storage_space_id = StorageSpace::factory()->create()->id;

        return [
            'machine_id' => $machine_id,
            'storage_space_id' => $storage_space_id,
        ];
    }
}
