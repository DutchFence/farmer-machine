<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Machine>
 */
class MachineFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user_id =  optional(User::inRandomOrder()->first())->id ??
        User::factory()->create()->id;
        
        return [
            'name' => fake()->name(),
            'user_id' => $user_id,
        ];
    }
}
