<?php

namespace Database\Factories;

use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $category_id = optional(ProductCategory::inRandomOrder()->first())->id ??
        ProductCategory::factory()->create()->id;
        return [
            'name' => fake()->unique()->name(),
            'SKU' => fake()->unique()->text(10),
            'price' => fake()->randomFloat(2, 0.00, 10),
            'description' => fake()->text(),
            'requires_cooling' => fake()->boolean(),
            'category_id' => $category_id,
        ];
    }
}
