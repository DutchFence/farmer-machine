<?php

namespace Database\Seeders;

use App\Models\MachineStorageSpace;
use Illuminate\Database\Seeder;

class MachineStorageSpaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MachineStorageSpace::factory()->count(100)->create();
    }
}
