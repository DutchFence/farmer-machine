<?php

namespace Database\Seeders;

use App\Models\Coin;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CoinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $coins = [
            [
                'value' => 5,
                'slug' => '5 cent'
            ],
            [
                'value' => 10,
                'slug' => '10 cent'
            ],
            [
                'value' => 20,
                'slug' => '20 cent'
            ],
            [
                'value' => 50,
                'slug' => '50 cent'
            ],
            [
                'value' => 100,
                'slug' => '1 euro'
            ],
            [
                'value' => 200,
                'slug' => '2 euro'
            ],
            [
                'value' => 500,
                'slug' => '5 euro'
            ],
            [
                'value' => 1000,
                'slug' => '10 euro'
            ],
            [
                'value' => 2000,
                'slug' => '20 euro'
            ],
        ];
        foreach ($coins as $coin) {
            Coin::create([
                'value' => $coin['value'],
                'slug' => $coin['slug'],
            ]);
        }
    }
}
