<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Product::create([
            'name' => 'aardbeien',
            'SKU' => 'doosje',
            'category_id' => 2,
            'description' => 'Mooi, gekke vorm en ook nog eens rood',
            'storage_property_id' => 2
        ]);
        Product::create([
            'name' => 'appels',
            'SKU' => 'zak',
            'category_id' => 2,
            'description' => 'Mooi, rond en ook nog eens rood',
            'storage_property_id' => 1
        ]);
        Product::create([
            'name' => 'rodekool',
            'SKU' => 'krop',
            'category_id' => 1,
            'description' => 'Mooi, rond en ook nog eens rood. Maar wel te groen. Groente.',
            'storage_property_id' => 2
        ]);
        Product::create([
            'name' => 'tomaten',
            'SKU' => 'doosje',
            'category_id' => 1, // Groenten
            'description' => 'Rode tomaten, sappig.',
            'storage_property_id' => 1 // Normaal vak
        ]);
        
        Product::create([
            'name' => 'komkommers',
            'SKU' => 'zak',
            'category_id' => 1, // Groenten
            'description' => 'Groene komkommer, langwerpig.',
            'storage_property_id' => 1 // Normaal vak
        ]);
        
        Product::create([
            'name' => 'paprika\'s',
            'SKU' => 'doosje',
            'category_id' => 1, // Groenten
            'description' => 'Rode paprika, zoet.',
            'storage_property_id' => 2 // Gekoeld vak
        ]);
        
        Product::create([
            'name' => 'courgettes',
            'SKU' => 'zak',
            'category_id' => 1, // Groenten
            'description' => 'Groene courgette, langwerpig.',
            'storage_property_id' => 1 // Normaal vak
        ]);
        
        Product::create([
            'name' => 'prei',
            'SKU' => 'zak',
            'category_id' => 1, // Groenten
            'description' => 'Witte prei, lang.',
            'storage_property_id' => 3 // Verwarmd vak
        ]);
        
        // Fruit
        Product::create([
            'name' => 'druiven',
            'SKU' => 'tros',
            'category_id' => 2, // Fruit
            'description' => 'Zoete blauwe druif, klein.',
            'storage_property_id' => 1 // Normaal vak
        ]);
        
        Product::create([
            'name' => 'kiwi\'s',
            'SKU' => 'doosje',
            'category_id' => 2, // Fruit
            'description' => 'Bruine kiwi, harig.',
            'storage_property_id' => 2 // Gekoeld vak
        ]);
        
        Product::create([
            'name' => 'perziken',
            'SKU' => 'zak',
            'category_id' => 2, // Fruit
            'description' => 'Zachte perzik, oranje.',
            'storage_property_id' => 1 // Normaal vak
        ]);
        
        Product::create([
            'name' => 'ananassen',
            'SKU' => 'doosje',
            'category_id' => 2, // Fruit
            'description' => 'Zoete ananas, geel.',
            'storage_property_id' => 2 // Gekoeld vak
        ]);
        
        Product::create([
            'name' => 'sinaasappels',
            'SKU' => 'zak',
            'category_id' => 2, // Fruit
            'description' => 'Oranje sinaasappel, zoet.',
            'storage_property_id' => 1 // Normaal vak
        ]);
        
        Product::create([
            'name' => 'peren',
            'SKU' => 'doosje',
            'category_id' => 2, // Fruit
            'description' => 'Zoete peer, groen.',
            'storage_property_id' => 2 // Gekoeld vak
        ]);
        
        Product::create([
            'name' => 'mango\'s',
            'SKU' => 'zak',
            'category_id' => 2, // Fruit
            'description' => 'Rijpe mango, geel.',
            'storage_property_id' => 1 // Normaal vak
        ]);
        
        Product::create([
            'name' => 'bananen',
            'SKU' => 'tros',
            'category_id' => 2, // Fruit
            'description' => 'Gele banaan, zoet.',
            'storage_property_id' => 3 // Verwarmd vak
        ]);
        
      
        
    }
}
