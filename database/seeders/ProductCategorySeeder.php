<?php

namespace Database\Seeders;

use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ProductCategory::create([
            'name' => 'groente',
            'description' => 'Gezond en fris en groen en vies'
        ]);
        ProductCategory::create([
            'name' => 'fruit',
            'description' => 'Gezond en fris en zoet'
        ]);
    }
}
