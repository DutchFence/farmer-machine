<?php

namespace Database\Seeders;

use App\Models\StorageProperty;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StoragePropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        StorageProperty::create([
            'type' => 'standaard',
            'description' => 'Standaard vak zonder koeling of verwarming',
        ]);

        StorageProperty::create([
            'type' => 'gekoeld',
            'description' => 'Gekoeld vak van 2 tot 7 graden celsius',
        ]);

        StorageProperty::create([
            'type' => 'verwarmd',
            'description' => 'Verwarmd vak van 10 tot 18 graden celsius',
        ]);

    }
}
