<?php

use App\Models\User;

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CashController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MachineController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShoppingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductPriceController;
use App\Http\Controllers\ManageMachineController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->group(function () {
    Route::get('/', function () {
        return Inertia::render('Welcome');
    })->name('home');
    Route::get('/login', function () {
        return Inertia::render('Auth/Login', [
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
        ]);
    });
    Route::get('/register', function () {
        return Inertia::render('Auth/Register', [
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
        ]);
    });
    Route::post('/register', function () {
        return Inertia::render('Auth/Register', [
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
        ]);
    });
});

Route::get('/about', function () {
    return Inertia::render('Company/About');
});
Route::get('/catalog', function () {
    return Inertia::render('Company/Catalog');
});
Route::get('/contact', function () {
    return Inertia::render('Company/Contact');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('Dashboard');
    Route::get('/machine/create', [MachineController::class, 'create']);
    Route::get('/machine/{id}/cash', [CashController::class, 'show']);
    Route::get('/machine/{id}/shopping', [ShoppingController::class, 'show']);
    Route::get('/machine/{id}/{name}', [MachineController::class, 'show']);
    Route::get('/products', [ProductController::class, 'index']);
    Route::put('/products/prices/{id}', [ProductPriceController::class, 'update']);
    Route::post('/machine/create', [MachineController::class, 'store']);
    Route::get('/machine/{id}/{name}/manage', [ManageMachineController::class, 'show']);
    Route::put('/storage/manage/{storageId}/switch', [ManageMachineController::class, 'switchProduct']);
    Route::put('/storage/manage/{storageId}/latch', [ManageMachineController::class, 'latch']);
    Route::put('/storage/manage/{storageId}/stock', [ManageMachineController::class, 'stock']);
    Route::put('/machine/{id}/cash/exchange', [CashController::class, 'exchange']);
    Route::put('/machine/{id}/shopping/cash-payment', [PaymentController::class, 'cash']);
    Route::put('/machine/{id}/shopping/card-payment', [PaymentController::class, 'card']);
    Route::get('/users-panel', [UserController::class, 'index'])->name('users.index')->can('viewAny', User::class);
    Route::put('/admin/manage/users/{userId}/role/switch', [UserController::class, 'updateRole']);
});
